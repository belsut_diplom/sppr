var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var webLog = require('./bin/log')('web');
var log = require('./bin/log')(module);
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressSession = require('express-session');

var routes = require('./bin/routes');
var conf = require('./config');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(function (req, res, next) {
  // this properties can be changed after in any middleware
  var uriPath = req.path;
  var method = req.method;

  next();
  webLog.verbose(res.statusCode + ' ' + method + ' ' + uriPath);
});
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(expressSession(conf.web.session));

app.use(conf.web.uri, function (req, res, next) {
  res.locals.path = path.posix.normalize(conf.web.uri + '/' + req.path + '/');
  routes(req, res, next);
});
app.use(express.static(path.join(__dirname, 'public')));


// catch 404 and forward to error handler if no middleware was found
// app.use(function (req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// common error handler

app.post(function(err, req, res, next){
  res.send({status: "ERROR", code: err.status, msg: err.msg});
  next();
});

if (conf.env === 'dev') {
  app.get(function (err, req, res, next) {
    if(err){
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err
      }, next);
    }
    else{
      next();
    }
  });
} else {
  app.get(function (err, req, res, next) {
    if(err) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {}
      }, next);
    }
    else{
      next();
    }
  });
}


module.exports = app;

log.info('Application loaded.');
