var myName = "D&D,  inc.";

for(var i = 0; i < 5000; ++i) Math.random();

var red = [0, 100, 63];
var orange = [40, 100, 60];
var green = [75, 100, 40];
var blue = [196, 77, 55];
var purple = [280, 50, 60];
var letterColors = shuffle([red, orange, green, blue, purple]);

function shuffle(a) {
  for (var i = a.length; i; --i) {
    var j = Math.floor(Math.random() * i);
    var x = a[i - 1];
    a[i - 1] = a[j];
    a[j] = x;
  }
  return a;
}

drawName(myName, letterColors);


bubbleShape = 'circle';
// bubbleShape = 'square';

bounceBubbles();