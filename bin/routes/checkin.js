var log = require('../log')(module);
var Status = require('../models/status');
var Action = require('../models/action');
var async = require('async');

module.exports.post = function (req, res, next) {
  var user = req.session.user;
  if(!user){
    log.error("Unexpected situation, no user in session!");
    req.session.destroy(function (err) {
      if(err) log.error(err);
      next(new Error(500));
    });
    return;
  }

  async.waterfall([
    function (callback) {
      Status.findOne(
        {station_name: user.station_name}, function (err, status) {
          err = handleAndReduceErr(err, status, "Can't find status for station: " + user.station_name);
          if(err){
            callback(err);
          }
          else{
            callback(null, status);
          }
        });
    },
    function (status, callback) {
      var query = {
        role_name: user.role_name,
        situation_name: status.situation_name,
        station_name: user.station_name
      };
      Action.findOne(query, function (err, action) {
        err = handleAndReduceErr(err, action, "Can't find action by query: " + query);
        if(err){
          callback(err);
        }
        else{
          callback(null, action);
        }
      });
    }
  ],
  function(err, action){
    err = handleAndReduceErr(err, action, "Can't find action");
    if(err){
      next(err);
    }else{
      res.send({situation: action.situation_name, actions: action.actions});
      next();
    }
  })
};

function handleAndReduceErr(err, bean, msg) {
  if(err){
    log.error(err);
    return err;
  }
  if(!bean){
    err = new Error(msg);
    log.warn(err);
    return err;
  }
  return null;
}