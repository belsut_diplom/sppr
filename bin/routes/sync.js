var log = require('../log')(module);
var Status = require('../models/status');
var async = require('async');
var Situation = require('../models/situation');

module.exports.post = function (req, res, next) {
  if(!req.body){
    next(new Error("400, No request body found."));
    return;
  }
  var station = req.body.station;
  var status = req.body.status || {};

  if(!station){
    next(new Error('400, Station is absent or null.'))
  }
  else{
    async.waterfall([
      function(callback){
        Situation.findOne(
          {station_name: station, status: status}, function (err, situation) {
            if(err){
              log.error(err);
              callback(err);
            }
            else if(!situation){
              callback(new Error("No such situation find."));
            }
            else{
              callback(null, situation);
            }
          })
      },
      function (situation, callback) {
        Status.update(
          {station_name: station}, {situation_name: situation.name}, {upsert: true},
            function (err, raw) {
              if(err){
                log.error(err);
                callback(err);
              }
              else{
                callback();
              }
            });
      }
    ],
    function (err, results) {
      if(err){
        log.error(err);
        next(err);
      }
      else {
        res.send({status: 'OK'});
        next();
      }
    });
  }
};