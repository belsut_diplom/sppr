var log = require('../log')(module);
var User = require('../models/user');

//fake authorisation
module.exports.check = function (req, res, next) {
  if (! req.session || !req.session.auth) {
    next(new Error("Unauthorized access, 403!"));
  }
  else{
    next();
  }
};

module.exports.authorize = function (req, res, next) {
  var login = req.body.login;
  var password = req.body.password;

  var user = new User({
    login: login,
    password: password
  },{
    autoIndex: false
  });

  // пишу как быдло..
  user = {
    login: user.login,
    passwordHash: user.passwordHash,
    passwordSalt: user.passwordSalt
  };

  User.findOne(user, function (err, realUser) {
    if(err){
      next(err);
      return;
    }
    if(!realUser){
      next(new Error("404, Can't find user: " + user));
      return;
    }

    req.session.auth = true;
    req.session.user = realUser;
    // user role is a role on railway station
    log.info('New authorisation: ' + req.session.user.login);
    res.send(realUser);
    next();
  });
};