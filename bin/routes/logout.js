module.exports.post = function (req, res, next) {
  req.session.destroy(function (err) {
    if(!err) res.send({status: "OK"});
    else res.send({status: "ERROR"});
    next(err);
  })
};