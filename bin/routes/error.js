var conf = require("../../config");
var log = require('../log')(module);

module.exports.get = (conf.env === 'dev') ?
  function (err, req, res, next) {
    log.error(err);
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  }
  :
  function (err, req, res, next) {
    console.log("!1");
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  };


module.exports.post = function (err, req, res, next) {
  if(err) {
    console.log("!2");
    res.status(err.status || 500);
    res.send({status: "error"});
    next();
  }
  else{
    next();
  }
};

