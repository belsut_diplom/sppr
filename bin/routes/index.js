var express = require('express');
var router = express.Router();

var actions = require('./actions');
var situation = require('./situations');
var control = require('./control');
var error = require('./error');
var auth = require('./auth');
var sync = require('./sync');
var checkin = require('./checkin');
var logout = require('./logout');


router.get('/', function (req, res, next) {
  res.render('index', {title: 'Main page'});
});

router.post("/login", auth.authorize);
router.use(auth.check);
router.post('/action', actions.post);
router.post('/situation', situation.post);
router.post('/control', control.post);
router.post('/sync', sync.post);
router.post('/checkin', checkin.post);
router.post('/logout', logout.post);

// if some error was generated in previous middlewares
router.post(error.post);
router.use(error.get);

module.exports = router;
