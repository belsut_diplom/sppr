var mongoose = require('mongoose');
var connection = require('../db');

// current vector for stations
var schema = new mongoose.Schema({
  station_name: {
    type: String,
    unique: true,
    require: true
  },
  situation_name: {
    type: String,
    require: true
  }
});

var Event = connection.model('Status',schema);

module.exports = Event;