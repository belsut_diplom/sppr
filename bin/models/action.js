var mongoose = require('mongoose');
var connection = require('../db');

var schema = new mongoose.Schema({
  role_name: {
    type: String,
    required: true
  },
  situation_name: {
    type: String,
    required: true
  },
  station_name: {
    type: String,
    required: true
  },
  actions: Array
});

var Action = connection.model('Action',schema);

module.exports = Action;