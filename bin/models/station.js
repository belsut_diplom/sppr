var mongoose = require('mongoose');
var connection = require('../db');

var schema = new mongoose.Schema({
  name: {
    type: String,
    require: true
  },
  short: String,
  info: mongoose.Schema.Types.Mixed
});

schema.virtual('title').get(function () {
  return this.short | this.name;
});
schema.virtual('title').set(function (newVal) {
  this.short = newVal;
});


var Station = connection.model('Station',schema);

module.exports = Station;