var mongoose = require('mongoose');
var connection = require('../db');

var schema = new mongoose.Schema({
  station_name : {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  description: String,
  status: mongoose.Schema.Types.Mixed
});


var Situation = connection.model('Situation',schema);

module.exports = Situation;