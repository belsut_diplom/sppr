var mongoose = require('mongoose');
var connection = require('../db');

var schema = new mongoose.Schema({
  name: {
    type: String,
    unique: true
  },
  description: String

});

var Role = connection.model('Role',schema);


module.exports = Role;