var mongoose = require('mongoose');
var connection = require('../db');
var async = require('async');

var schema = new mongoose.Schema({
  login : {
    type: String,
    unique: true,
    required: true
  },
  station_name : {
    // type: mongoose.Schema.Types.ObjectId,
    type: String,
    required: true
  },
  role_name : {
    type: String,
    required: true
  },
  passwordHash : {
    type: String,
    required: true
  },
  passwordSalt : {
    type: String,
    required: true
  },
  personal: {
    age: Number,
    name: String
  }
});

schema.virtual('password').get(function () {
  return this.passwordHash;
}).set(function (newVal) {
  this.passwordHash = newVal;
  this.passwordSalt = "No hashing yet...";
});

schema.methods.sayHello = function () {
  console.log("Hello from " + this.login);
};
var User = connection.model('User',schema);

module.exports = User;