var winston = require('winston');
var conf = require('../config');

//that's not for your mind, baby ;)
var rootDepth = require('path').normalize(__dirname + "/..").toString().split(/[\\\/]/).length;

module.exports = function (module) {
  if(!module) module = {};
  var path;
  if(module.filename) {
    path = module.filename.split(/[\\\/]/);
    path = path.slice(rootDepth - path.length).join('/');
  }
  else{
    path = module;
  }

  var wlog = new winston.Logger({
    transports: [
      new winston.transports.Console({
        colorize: true,
        level: conf.logging.level,
        label: path
      }),
      new (winston.transports.File)({
        name: 'info-file',
        filename: 'logs/verbose.log',
        level: 'verbose'
      }),
      new (winston.transports.File)({
        name: 'error-file',
        filename: 'logs/warn.log',
        level: 'warn'
      })
    ]
  });

//{ error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
  wlog.debug = function (arg1, arg2, arg3, arg4) {
    wlog.log('debug', arg1);
  };
  wlog.error = function (arg1, arg2, arg3, arg4) {
    wlog.log('error', arg1);
  };

  return wlog;
};

