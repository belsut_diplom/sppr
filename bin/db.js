var mongoose = require('mongoose');
mongoose.set("debug", true);
var conf = require('../config');


//
// var db = mongoose.connection;
//
// db.once('open', function() {
//   console.log("connected");
// });


module.exports = mongoose.createConnection(conf.db.url);
