var conf = require('../config')["db"];

var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');
// Connection URL
var url = conf.testUrl;
var collectionName = conf.collections.test;

// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");

  //callback hell
  truncateCollection(db, function () {
    insertDocuments(db, function () {
      updateDocument(db, function () {
        db.close();
      });
    });
  });
});


var insertDocuments = function (db, callback) {
  // Get the documents collection
  var collection = db.collection(collectionName);
  // Insert some documents
  collection.insertMany([
    {a: 1}, {a: 2, c: "Wow"}, {a: "Flexible"}, {a: 1, c: "Hello World!"}
  ], function (err, result) {
    if (err) throw err;
    assert.equal(err, null);
    assert.equal(4, result.result.n);
    assert.equal(4, result.ops.length);
    console.log("Inserted 4 documents into the '" + collectionName + "' collection");
    echoCollection(collection, callback);
  });
};

var updateDocument = function (db, callback) {
  // Get the documents collection
  var collection = db.collection(collectionName);
  // Update document where a is 2, set b equal to 1
  collection.updateOne({a: 2}
    , {$set: {b: 2016}}, function (err, result) {
      if (err) throw err;
      assert.equal(err, null);
      assert.equal(1, result.result.n);
      console.log("Updated the document with the field a equal to 2016");
      echoCollection(collection, callback);
    });
};

function truncateCollection(db, callback) {
  var collection = db.collection(collectionName);
  collection.drop(callback);
}

function echoCollection(collection, callback) {
  var cursor = collection.find({a: 1});

  cursor.toArray(function (err, results) {
    if (err) throw err;
    console.log(results);
    callback(results);
  });
  
  // collection.findOne({a: 1, c: 'Hello World!'}, function (err, val, callback) {
  //   console.log(val);
  // });
}
