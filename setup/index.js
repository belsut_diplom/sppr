var data = require('./demoDB.json');

var async = require('async');

var connection = require('../bin/db');

var User = require('../bin/models/user');
var Role = require('../bin/models/role');
var Station = require('../bin/models/station');
var Status = require('../bin/models/status');
var Situation = require('../bin/models/situation');
var Action = require('../bin/models/action');

// User.remove({}, throwError);

async.series([
  dropRoles,
  dropStations,
  dropUsers,
  dropSituations,
  dropStatuses,
  dropActions,
  createRoles,
  createStations,
  createUsers,
  createSituations,
  createStatuses,
  createActions
], finish);

function dropRoles(callback) {
  Role.remove({}, callback);
}

function dropUsers(callback) {
  User.remove({}, callback);
}

function dropStations(callback) {
  Station.remove({}, callback);
}

function dropStatuses(callback) {
  Status.remove({}, callback);
}

function dropSituations(callback) {
  Situation.remove({}, callback);
}

function dropActions(callback) {
  Action.remove({}, callback);
}

function createRoles(callback) {
  async.each(data.roles, function (val, callback) {
    var role = new Role(val);
    role.save(callback);
  }, callback);
}

function createStations(callback) {
  async.each(data.stations, function (val, callback) {
    var station = new Station(val);
    station.save(callback);
  }, callback);
}

function createUsers(callback) {
  async.each(data.users, function (val, callback) {
    var user = new User(val);
    user.save(callback);
  }, callback);
}

function createSituations(callback) {
  async.each(data.situations, function (val, callback) {
    var situation = new Situation(val);
    situation.save(callback);
  }, callback);
}

function createStatuses(callback) {
  async.each(data.status, function (val, callback) {
    var status = new Status(val);
    status.save(callback);
  }, callback);
}

function createActions(callback) {
  async.each(data.actions, function (val, callback) {
    var action = new Action(val);
    action.save(callback);
  }, callback);
}

function finish(err) {
  if(err) console.log(err);
  connection.close(function(err){
    if(err) throw err;
  });
}